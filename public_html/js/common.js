$(document).ready(function() {

//Цели для Яндекс.Метрики и Google Analytics
$(".count_element").on("click", (function() {
	ga("send", "event", "goal", "goal");
	yaCounterXXXXXXXX.reachGoal("goal");
	return true;
}));

//SVG Fallback
if(!Modernizr.svg) {
	$("img[src*='svg']").attr("src", function() {
		return $(this).attr("src").replace(".svg", ".png");
	});
};

//Попап менеджер FancyBox
//Документация: http://fancybox.net/howto
//<a class="fancybox"><img src="image.jpg" /></a>
//<a class="fancybox" data-fancybox-group="group"><img src="image.jpg" /></a>
$(".fancybox").fancybox();

//Каруселька
//Документация: http://owlgraphic.com/owlcarousel/
// Главная, слайдер топ
var owl_main = $(".owl-carousel-main");
owl_main.owlCarousel({
	loop:true,
	margin:0,
	nav:false,
	dots:true,
	dotsEach:true,
	mouseDrag: false,
	autoplay: true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		1000:{
			items:1
		}
	}

});
owl_main.on("mousewheel", ".owl-wrapper", function (e) {
	if (e.deltaY > 0) {
		owl_main.trigger("owl.prev");
	} else {
		owl_main.trigger("owl.next");
	}
	e.preventDefault();
});
$(".next-button-main").click(function(){
	owl_main.trigger("owl_main.next");
});
$(".prev-button-main").click(function(){
	owl_main.trigger("owl.prev");
});

// Календарь jQueryUI 
// <input id="datepicker">
$( "#datepicker" ).datepicker();
$.datepicker.setDefaults($.datepicker.regional['ru']);

//Аякс отправка форм
//Документация: http://api.jquery.com/jquery.ajax/
$("#form").submit(function() {
	$.ajax({
		type: "POST",
		url: "mail.php",
		data: $(this).serialize()
	}).done(function() {
		alert("Спасибо за заявку!");
		setTimeout(function() {

			$("#form").trigger("reset");
		}, 1000);
	});
	return false;
});

//Chrome Smooth Scroll
try {
	$.browserSelector();
	if($("html").hasClass("chrome")) {
		$.smoothScroll();
	}
} catch(err) {

};

$("img, a").on("dragstart", function(event) { event.preventDefault(); });

$('#tab-content .tab-view:not(:first)').hide();

$('#info-nav li').click(function(event) {
	event.preventDefault();
	$('#tab-content .tab-view').hide();
	$('#info-nav .current').removeClass("current");
	$(this).addClass('current');

	var clicked = $(this).find('a:first').attr('href');
	$('#tab-content ' + clicked).fadeIn('fast');
}).eq(0).addClass('current');

});

// $(window).load(function() {

// 	$(".loader_inner").fadeOut();
// 	$(".loader").delay(400).fadeOut("slow");

// });
